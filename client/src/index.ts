declare var $: any

export const start = function () {
    console.log( "index::starting" )

    fetch( "http://localhost:8081/greeting" )
        .then( ( response ) => response.json() )
        .then( function ( greeting ) {
            $( "#greeting" ).html( greeting.text )
        } )
}
