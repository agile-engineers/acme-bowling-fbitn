const { assert } = require( "chai" )
const { remote } = require( "webdriverio" )

exports.initializeWebDriver = function () {
    return remote( { desiredCapabilities: { browserName: "chrome" } } ).init()
}

exports.loginAs = ( credentials ) => function () {
    return this.click( "#sign-in" )
        .setValue( "#email", credentials.email )
        .setValue( "#password", credentials.password )
        .click( "button" )
}

exports.assertTextAs = ( selector, expected ) => function () {
    return this.getText( selector ).then( actual => assert.equal( actual, expected ) )
}

exports.assertListAs = ( selector, expected ) => function () {
    return this.getText( selector ).then( actual => assert.deepEqual( actual, expected ) )
}

exports.assertCountAs = ( selector, expected ) => function () {
    return this.elements( selector ).then( actual => {
        assert.equal( actual.value.length, expected )
    } )
}

exports.cleanupWebDriver = function () {
    return this.driver.end()
}

exports.takeScreenShot = function () {
    return this.saveScreenshot( `${ process.cwd() }/tmp/screenshots/${ Date.now() }.png` )
}

exports.users = {
    "joe": { email: "joe@m.com", password: "pass" }
}
