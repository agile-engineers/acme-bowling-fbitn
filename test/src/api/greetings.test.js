const { assert } = require( "chai" )
const { suite, test } = require( "mocha" )

const { Assertions, URLClient } = require( "./utils" )

suite( "greetings", function () {
    const url = URLClient.withBaseURL( "http://localhost:8081" )

    test( "default message", function () {
        return url.get( "/greeting" )
            .then( Assertions.responseEquals( { text: "Hello, World!" } ) )
    } )
} )
