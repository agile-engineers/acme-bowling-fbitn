import * as Express from "express"
import * as CORS from "cors"
import * as BodyParser from "body-parser"

const port = process.env.PORT || 8081
const app = Express()

app.use( CORS( {
    origin: "http://localhost:8080",
    credentials: true,
} ) )

app.use( BodyParser.json() )

app.get( "/greeting", function ( req, res ) {
    res.send( { message: "hello, world" } )
} )

app.listen( port, function () {
    console.log( "api server is started: " + port )
} )
