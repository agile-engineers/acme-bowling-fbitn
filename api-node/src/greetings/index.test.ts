import { assert } from "chai"
import { suite, test } from "mocha"

import { Greeting, GreetingService } from "./index"

suite( "greeting service", function () {

    test( "build default greeting", function () {
        // given
        const sut = new GreetingService()

        // when
        const result: Greeting = sut.build()

        // then
        assert.equal( result.text, "Hello, World!" )
    } )
} )
