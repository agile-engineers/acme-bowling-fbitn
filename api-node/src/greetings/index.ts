export class Greeting {
    text: string
}

export class GreetingService {
    build () {
        return { text: "Hello, World!" };
    }
}