using ApiWeb.Greetings;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        [Fact]
        public void build_a_default_greeting()
        {
            // given
            GreetingService sut= new GreetingService();

            // when
            Greeting result = sut.Build();

            // then
            Assert.Equal("Hello, World!", result.Text);
        }
    }
}