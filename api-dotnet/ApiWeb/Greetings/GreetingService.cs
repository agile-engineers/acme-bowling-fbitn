﻿namespace ApiWeb.Greetings
{
    public class Greeting
    {
        public string Text { get; set; }
    }

    public class GreetingService
    {
        public Greeting Build()
        {
            return new Greeting {Text = $"Hello, World!"};
        }
    }
}