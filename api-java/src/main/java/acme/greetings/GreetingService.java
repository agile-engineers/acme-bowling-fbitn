package acme.greetings;

public class GreetingService {
    public Greeting build() {
        return new Greeting("Hello, World!");
    }
}
