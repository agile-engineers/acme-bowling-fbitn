package acme.greetings;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public Greeting getGreeting() throws Exception {
        GreetingService greetingService = new GreetingService();
        return greetingService.build();
    }
}
